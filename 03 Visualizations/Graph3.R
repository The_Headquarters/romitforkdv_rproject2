require("jsonlite")
require("RCurl")
require("ggplot2")
# Change the USER and PASS below to be your UTEid
teams <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from teams where SB is not null"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_rb36494', PASS='orcl_rb36494', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE), ))

require(extrafont)
ggplot() + 
  coord_cartesian() + 
  scale_x_continuous() +
  scale_y_continuous() +
  #facet_wrap(~SURVIVED) +
  #facet_grid(.~SURVIVED, labeller=label_both) + # Same as facet_wrap but with a label.
  #facet_grid(PCLASS~SURVIVED, labeller=label_both) +
  labs(title='Baseball') +
  labs(x="Batters Hit", y="Home Runs") +
  
  layer(data=teams, 
        mapping=aes(x=as.numeric(as.character(HBP)), y=as.numeric(as.character(HR))),  
        geom="point",
        #geom_params=list(), 
        stat="identity", 
        #stat_params=list(),
        #position=position_identity()
        position=position_jitter(width=0.3, height=0)
  )